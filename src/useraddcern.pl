#!/usr/bin/perl -w

#
# lookup CERN LDAP for user info
#
# v. 0.4 13 Feb 2017 Jaroslaw.Polok <jaroslaw.polok@cern.ch>
#                   - add check for shell existence.
# v. 0.1 27 Oct 2008 Jaroslaw.Polok <jaroslaw.polok@cern.ch>

use strict;
use Getopt::Long;
use Pod::Usage;

eval("use Net::LDAP");  
if($@){ printf STDERR "Error: Missing Net::LDAP module. Please install perl-LDAP\n"; exit 1;}            

my $shortusage="Usage: useraddcern LOGIN\n See: useraddcern --help and man useraddcern for more information\n";
my ($help,$login,$debug,$shell,$directory);


my %opts=(
 "login=s"       => \$login,
 "verbose"       => \$debug,
 "help"          => \$help,
 "shell"	 => \$shell,
 "directory"	 => \$directory,
);

my $search = "";
my $ok = 0;
my $excode = 0;
my $options = GetOptions(%opts);

pod2usage(1) if $help;

if ($< != 0) { print "You must be root in order to add users\n"; exit 1; }

while (@ARGV){
$search.="(cn=$ARGV[0])";$ok+=1;shift @ARGV;
} 
$search="(|".$search.")" if $ok>1;

$search.="(cn=$login)",$ok+=1 if $login;
if(!$ok) { printf $shortusage;exit 1;}
$search.="(objectClass=organizationalPerson)";$ok+=1;
$search="(&".$search.")" if $ok > 1;

printf "LDAP search: %s\n",$search if $debug;

my $ldap = Net::LDAP->new ( "xldap.cern.ch" ) or die "Error connecting to LDAP: $@\n";
my $mesg = $ldap->bind ( version => 3 ) 
			 or die "Error binding to LDAP: $@\n";
my $result = $ldap->search ( base    => "ou=users,ou=organic units,dc=cern,dc=ch",
			     scope   => "sub",
			     filter  => "$search",
			     attrs   => "") or die "Error searching LDAP: $@\n";
my @entries = $result->entries;
my $entr;

printf "LDAP return: %d entries.\n",$#entries +1 if $debug;
die "No match in user database.\n" if ($#entries < 0);

my @allgids= ();
my @allusers= ();
my @allgroups= ();
 
foreach $entr ( @entries ) {
   my $attr;
   my %user=( cn => undef, loginShell => undef, uidNumber => undef, gidNumber => undef, unixHomeDirectory => undef, gecos => undef );
   foreach $attr ( sort $entr->attributes ) {
     next if ( $attr =~ /;binary$/ );
     for ($attr) {
	/^cn$/             
		&& do { $user{cn}=$entr->get_value ( $attr ); last;};
	/^loginShell$/                
		&& do { $user{loginShell}=$entr->get_value ( $attr ); last;};
	/^uidNumber$/              
		&& do { $user{uidNumber}=$entr->get_value ( $attr ); last;};
	/^gidNumber$/              
		&& do { $user{gidNumber}=$entr->get_value ( $attr ); push(@allgids,$user{gidNumber}); last;};
	/^unixHomeDirectory$/              
		&& do { $user{unixHomeDirectory}=$entr->get_value ( $attr ); last;};
	/^gecos$/                
		&& do { $user{gecos}=$entr->get_value ( $attr );
			$user{gecos}=~s/\,Bld\..*$// ;
			$user{gecos}=~s/\,\,\,$//; last;}; # why oh why gecos contains phone number and office ??
     }
   }
   if (!$user{cn} || !$user{loginShell} || !$user{uidNumber} || !$user{gidNumber} || !$user{unixHomeDirectory} || ! $user{gecos}) {
        if ($user{cn}) {
        	printf STDERR "Incomplete LDAP data for: $user{cn}, skipping record.\n";
                $excode=1;
		} else {
		printf STDERR "LDAP data incoherent (object is NOT an 'organizationalPerson' ?), skipping record.\n";
                $excode=1;
		} 
	next;
	}		
   
   push(@allusers,{%user});
 }


$search="";
$ok=0;

foreach my $gid ( @allgids ) {
 $search.="(gidNumber=$gid)";$ok+=1;
}

$search="(|".$search.")" if $ok > 1;
$search.="(objectClass=group)";$ok+=1;
$search="(&".$search.")" if $ok > 1;
printf "LDAP search: %s\n",$search if $debug;

$result = $ldap->search ( base    => "ou=unix,ou=workgroups,dc=cern,dc=ch",
			  scope   => "sub",
			  filter  => "$search",
			  attrs   => "") or die "Error searching LDAP: $@\n";

@entries = $result->entries;

printf "LDAP return: %d entries.\n",$#entries +1 if $debug;
die "No match in group database.\n" if ($#entries < 0);

foreach $entr ( @entries ) {
   my $attr;
   my %group=( cn => undef , gidNumber => undef );
   foreach $attr ( sort $entr->attributes ) {
     next if ( $attr =~ /;binary$/ );
     for ($attr) {
	/^cn$/             
		&& do { $group{cn}=$entr->get_value ( $attr ); last;};
	/^gidNumber$/                
		&& do { $group{gidNumber}=$entr->get_value ( $attr ); last;};
     }
   }
   if (!$group{cn} || !$group{gidNumber}) {
   	if ($group{cn}) {
        	printf STDERR "Incomplete LDAP data for: $group{cn}, skipping record.\n";
                $excode=1;
		} else {
		printf STDERR "LDAP data incoherent (object is NOT an 'group' ?), skipping record.\n";
                $excode=1;
		} 
	next;
	}		
   	   
   push(@allgroups,{%group});
 }

  
	
  


for my $group (@allgroups) {
 my $command = "/usr/sbin/groupadd -g \"$group->{gidNumber}\" -f \"$group->{cn}\"";
 printf "running: $command\n" if $debug;
 system($command);
 die "Failed to execute: $command\n" if ($? == -1);
 die "Error executing command\n" if ($? >> 8);
}		


for my $user (@allusers) {
 my $command = "/usr/sbin/useradd -c \"$user->{gecos}\" -d \"$user->{unixHomeDirectory}\" -g \"$user->{gidNumber}\" -s \"$user->{loginShell}\" -u \"$user->{uidNumber}\" -M -n \"$user->{cn}\"";
 if ($directory ) {
	my $command = "/usr/sbin/useradd -c \"$user->{gecos}\" -d \"$user->{unixHomeDirectory}\" -g \"$user->{gidNumber}\" -s \"$user->{loginShell}\" -u \"$user->{uidNumber}\" -M -n \"$user->{cn}\"";
 }
 printf "running: $command\n" if $debug;
 if (! -x $user->{loginShell}) {
	printf STDERR "WARN: user shell ($user->{loginShell}) not installed on this system.\n";
	my $inst_command = "/usr/bin/yum -y install \"$user->{loginShell}\"" if $shell;
	printf "INFO: installing user shell $user->{loginShell}\n" if $shell;
	system($inst_command) if $shell;
 }
 system($command);
 die "Failed to execute: $command\n" if ($? == -1);
 die "Error executing command.\n" if ($? >> 8);
}	  

exit $excode;

__END__

=pod

=head1 NAME

addusercern - Utility to add CERN AFS accounts to the system.

=head1 DESCRIPTION

addusercern is a command line tool to ease adding of CERN accounts
to local system setup.
It will search CERN LDAP database for user and group information and 
add system accounts according to search criteria defined.

=head1 SYNOPSIS

=over 2

addusercern [--help]
  
addusercern [--shell] [--directory] [--login] LOGINID [.. LOGINID]
  
=back

=head1 OPTIONS

=over 4

=item B<--help>

Shows this help desription

=item B<--login> LOGINID

Add user with given LOGINID

=item B<--shell>

Install user default shell if not present

=item B<--directory>

Use the I<unixHomeDirectory> configured in LDAP (usually AFS or EOS). Default is to
configure a B<local> homedirectory
     
=back

All options can be abbreviated to shortest distinctive lenght,
(first letter). Single minus preceding option name may be used 
instead of double one.

For wildcard search use * character (double quoted or back-slashed 
to avoid shell variable expansion: "*" or \* ).

Please note that search option arguments are case insensitive.

=head1 DESCRIPTION

addusercern performs a search in CERN LDAP database, according to 
search criteria defined by options and adds named users to the system.

=head1 EXAMPLES

useraddcern --login jpolok

useraddcern -l "*polok"

=head1 AUTHORS

Jaroslaw Polok <jaroslaw.polok@cern.ch>, Ben Morrice <ben.morrice@cern.ch>

=head1 KNOWN BUGS

No strict checking of adduser/addgroup return and/or output ... to be fixed.

Also we are eager to hear about any (other) bugs at linux.support@cern.ch

Number of returned results is limited to 2000. (this is rather a feature).

=cut
      

 


